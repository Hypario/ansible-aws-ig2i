#!/bin/bash

# make sure everything is updated
sudo yum update -y

# check if python3 is installed
if ! command -v python3 >/dev/null 2>&1; then
    echo "Python3 is not installed."
    echo "Installing python3..."
    sudo yum install python3 -y
fi


# check if ansible is installed
if ! [ -x "$(command -v ansible)" ]; then
  echo 'ansible is not installed.'
  echo "Installing ansible..."
  sudo yum install ansible -y

  sudo ansible-galaxy collection install amazon.aws
  sudo ansible-galaxy collection install community.docker
  sudo pip3 install boto
  sudo pip3 install boto3
  sudo pip3 install --upgrade requests
fi

ansible-playbook architecture.yml
ansible-playbook updates.yml -i instances.yaml
ansible-playbook webserver.yml -i apache.yaml
ansible-playbook docker.yml -i nginx.yaml
